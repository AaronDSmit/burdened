/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class BurdenedItem extends Item {

  /* -------------------------------------------- */
  /*  Item Properties                             */
  /* -------------------------------------------- */

  /**
   * Determine which ability score modifier is used by this item
   * @type {string|null}
   */
  get abilityMod() {
    const itemData = this.data.data;

    if (this.data.type === "weapon") {
      const wt = itemData.weaponType;
      if (["simpleM", "martialM"].includes(wt)) {
        return "str";
      }
      else if (["simpleR", "martialR"].includes(wt)) {
        return "wis";
      }
    }

    return null;
  }

  /**
 * Does the Item implement an attack roll as part of its usage
 * @type {boolean}
 */
  get hasAttack() {
    return ["mwak", "rwak", "msak", "rsak"].includes(this.data.data.actionType);
  }

  /* -------------------------------------------- */

  /**
   * Does the Item implement a damage roll as part of its usage
   * @type {boolean}
   */
  get hasDamage() {
    return !!(this.data.data.damage && this.data.data.damage.parts.length);
  }

  /* -------------------------------------------- */

  /**
   * Does the item provide an amount of healing instead of conventional damage?
   * @return {boolean}
   */
  get isHealing() {
    return (this.data.data.actionType === "heal") && this.data.data.damage.parts.length;
  }

  /* -------------------------------------------- */

  /**
   * Does the Item have a target
   * @type {boolean}
   */
  get hasTarget() {
    const target = this.data.data.target;
    return target && !["none", ""].includes(target.type);
  }

  /* -------------------------------------------- */

  /**
   * A flag for whether this Item is limited in it's ability to be used by charges or by recharge.
   * @type {boolean}
   */
  get hasLimitedUses() {
    let uses = this.data.data.uses || {};
    return !!uses.per && (uses.max > 0);
  }

  /* -------------------------------------------- */
  /*	Data Preparation														*/
  /* -------------------------------------------- */

  prepareData() {
    super.prepareData();

    // Get the Item's data
    const itemData = this.data;
    const actorData = this.actor ? this.actor.data : {};
    const data = itemData.data;
    const config = CONFIG.BURDENED;
    const labels = {};

    if (itemData.type === "armour") {
      let defenceValue = duplicate(data.defence);
      if (data.defence[0] === "=") {
        defenceValue = data.defence.slice(1);
      }

      labels.armour = `${defenceValue} ${game.i18n.localize("BURDENED.Defence")}`;
    }

    // Item Actions
    if (data.hasOwnProperty("actionType")) {
      // Damage
      let dam = data.damage || {};
      if (dam.parts) {
        labels.damage = dam.parts.map(d => d[0]).join(" + ").replace(/\+ -/g, "- ");
        labels.damageTypes = dam.parts.map(d => config.damageTypes[d[1]]).join(", ");
      }
    }

    // Assign labels
    this.labels = labels;
  }

  /* -------------------------------------------- */

  /**
   * Roll the item to Chat, creating a chat card which contains follow up attack or damage roll options
   * @return {Promise}
   */
  async roll({ configureDialog = true } = {}) {

  }



  /* -------------------------------------------- */
  /*  Chat Cards																	*/
  /* -------------------------------------------- */

  /**
   * Prepare an object of chat data used to display a card for the Item in the chat log
   * @param {Object} htmlOptions    Options used by the TextEditor.enrichHTML function
   * @return {Object}               An object of chat data to render
   */
  getChatData(htmlOptions) {
    const data = duplicate(this.data.data);
    const labels = this.labels;

    // Rich text description
    data.description = TextEditor.enrichHTML(data.description, htmlOptions);

    // Item type specific properties
    const props = [];
    const fn = this[`_${this.data.type}ChatData`];
    if (fn) fn.bind(this)(data, labels, props);

    // General equipment properties
    if (data.hasOwnProperty("equipped")) {
      props.push(
        game.i18n.localize(data.equipped ? "BURDENED.Equipped" : "BURDENED.Unequipped")
      );
    }

    // Filter properties and return
    data.properties = props.filter(p => !!p);
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for item type items
   * @private
   */
  _itemChatData(data, labels, props) {

  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for light type items
   * @private
   */
  _lightChatData(data, labels, props) {
    props.push(
      data.duration + " " + game.i18n.localize("BURDENED.Time.Hours")
    );
  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for armour type items
   * @private
   */
  _armourChatData(data, labels, props) {
    props.push(
      CONFIG.BURDENED.armourTypes[data.armourType],
      labels.armour || null
    );
  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for weapon type items
   * @private
   */
  _weaponChatData(data, labels, props) {
    let damage = labels.damage.split(" ");

    props.push(
      CONFIG.BURDENED.weaponTypes[data.weaponType],
      damage.length ? damage[0] + " " + labels.damageTypes + " " + game.i18n.localize("BURDENED.Damage") : null,
    );
  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for spell type items
   * @private
   */
  _spellChatData(data, labels, props) {

  }

  /* -------------------------------------------- */

  /**
   * Prepare chat card data for rune type items
   * @private
   */
  _runeChatData(data, labels, props) {

  }

  /* -------------------------------------------- */
}