﻿// Namespace Configuration Values
export const BURDENED = {};

BURDENED.abilities = {
  "str": "BURDENED.AbilityStr",
  "dex": "BURDENED.AbilityDex",
  "con": "BURDENED.AbilityCon",
  "int": "BURDENED.AbilityInt",
  "wis": "BURDENED.AbilityWis",
  "cha": "BURDENED.AbilityCha"
};

/**
 * The valid currency denominations supported by the burdened system
 * @type {Object}
 */
BURDENED.currencies = {
  "cp": "BURDENED.CurrencyCP",
  "sp": "BURDENED.CurrencySP",
  "gp": "BURDENED.CurrencyGP"
};

/**
 * Configure aspects of encumbrance calculation so that it could be configured by modules
 * @type {Object}
 */
BURDENED.encumbrance = {
  currencyPerSlot: 100,
  maxSlots: 20,
  speedPenalty: -20
};

/**
 * Define the set of types which a weapon item can take
 * @type {Object}
 */
BURDENED.weaponTypes = {
  "simpleM": "BURDENED.WeaponType.SimpleM",
  "simpleR": "BURDENED.WeaponType.SimpleR",
  "martialM": "BURDENED.WeaponType.MartialM",
  "martialR": "BURDENED.WeaponType.MartialR",
  "improv": "BURDENED.WeaponType.Improv",
  "siege": "BURDENED.WeaponType.Siege"
};

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
BURDENED.weaponProperties = {
  "one": "BURDENED.WeaponProperty.One",
  "two": "BURDENED.WeaponProperty.Two",
  "amm": "BURDENED.WeaponProperty.Amm",
  "fir": "BURDENED.WeaponProperty.Fir",
  "thr": "BURDENED.WeaponProperty.Thr"
};

/**
 * The set of equipment types for armour, clothing, and other objects which can ber worn by the character
 * @type {Object}
 */
BURDENED.armourTypes = {
  "light": "BURDENED.ArmourType.Light",
  "medium": "BURDENED.ArmourType.Medium",
  "heavy": "BURDENED.ArmourType.Heavy",
  "shield": "BURDENED.ArmourType.Shield",
  "clothing": "BURDENED.ArmourType.Clothing"
};

/**
 * Define the set of weapon property flags which can exist on a weapon
 * @type {Object}
 */
BURDENED.armourProperties = {
  "one": "BURDENED.WeaponProperty.One",
  "two": "BURDENED.WeaponProperty.Two",
  "amm": "BURDENED.WeaponProperty.Amm",
  "fir": "BURDENED.WeaponProperty.Fir",
  "thr": "BURDENED.WeaponProperty.Thr"
};

BURDENED.rarity = {
  "common": "BURDENED.RarityCommon",
  "uncommon": "BURDENED.RarityUncommon",
  "rare": "BURDENED.RarityRare",
  "veryrare": "BURDENED.RarityVeryRare",
  "legendary": "BURDENED.RarityLegendary",
}

/**
 * Define the list of teams
 * @type {Object}
 */
BURDENED.teams = {
  player: "BURDENED.Player",
  other: "BURDENED.Other",
  enemy: "BURDENED.Enemy"
};

BURDENED.colors = {
  player: "BURDENED.Green",
  other: "BURDENED.Yellow",
  enemy: "BURDENED.Red"
}

/**
 * Character alignment options
 * @type {Object}
 */
BURDENED.alignments = {
  'lawful': "BURDENED.AlignmentLawful",
  'neutrial': "BURDENED.AlignmentNeutral",
  'chaotic': "BURDENED.AlignmentChaotic",
};

/* -------------------------------------------- */

/**
 * Classification types for item action types
 * @type {Object}
 */
BURDENED.itemActionTypes = {
  "mwak": "BURDENED.ActionMWAK",
  "rwak": "BURDENED.ActionRWAK",
  "msak": "BURDENED.ActionMSAK",
  "rsak": "BURDENED.ActionRSAK",
  "heal": "BURDENED.ActionHeal",
  "util": "BURDENED.ActionUtil",
  "other": "BURDENED.ActionOther"
};

/* -------------------------------------------- */

// Damage Types
BURDENED.damageTypes = {
  "physical": "BURDENED.DamagePhysical",
  "fire": "BURDENED.DamageFire",
  "cold": "BURDENED.DamageCold",
  "lightning": "BURDENED.DamageLightning",
  "acid": "BURDENED.DamageAcid",
  "poison": "BURDENED.DamagePoison",
};

/* -------------------------------------------- */