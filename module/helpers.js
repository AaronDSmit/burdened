export const registerHelpers = async function () {
  // Handlebars template helpers
  Handlebars.registerHelper("inventorySlots", function (inventorySlots) {
    if (inventorySlots.used >= inventorySlots.value)
      return new Handlebars.SafeString('<span class="knave-encumbered">' + inventorySlots.used + "/" + inventorySlots.value + "</span>");
    else
      return new Handlebars.SafeString(inventorySlots.used + "/" + inventorySlots.value);
  });

  Handlebars.registerHelper("isItemBroken", function (item) {
    if (item.type === "spell")
      return (item.data.used === "true" || !item.data.spellUsable);
    else {
      if (item.data.quality)
        return item.data.quality.value <= 0;
      else
        return false;
    }
  });

  Handlebars.registerHelper("canImproveAbility", function (ability, options) {
    const data = options.data.root.data;

    if (data.hasOwnProperty("levelingUp") && data.levelingUp) {
      const improvementsRemaining = data.remainingAbilities > 0;
      const hasImprovedThisAbility = data.improvedAbilities.includes(ability);

      return improvementsRemaining && !hasImprovedThisAbility;
    }

    return false;
  });

  Handlebars.registerHelper("canImproveHealth", function (options) {
    const data = options.data.root.data;
    return data.hasOwnProperty("canImproveHealth") && data.canImproveHealth;
  });

  Handlebars.registerHelper("canSelectTalent", function (options) {
    const data = options.data.root.data;
    return data.hasOwnProperty("canSelectTalent") && data.canSelectTalent;
  });

  Handlebars.registerHelper("greaterThanZero", function (number) {
    return number > 0;
  });

  Handlebars.registerHelper("isdefined", function (value) {
    return value !== undefined;
  });

  Handlebars.registerHelper("isdefinedAndTrue", function (value) {
    return value !== undefined && value == true;
  });
}