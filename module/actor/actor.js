
/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class BurdenedActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags;

    this._prepareBaseStats(actorData);

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === "character") {
      this._prepareCharacterData(actorData);
    }
    else if (actorData.type === "npc") {
      this._prepareNPCData(actorData);
    }
    else if (actorData.type === "enemy") {
      this._prepareNPCData(actorData);
    }

    this._prepareAbilityScores(data);
  }

  _prepareAbilityScores(data) {
    data.attributes.armour.final = data.attributes.armour.value;
    data.attributes.armour.bonus = Number(data.attributes.armour.final) - Number(10);

    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(data.abilities)) {
      const abilityModifer = Object.values(ability.modifier).reduce((val, mod) => val += mod, 0);
      ability.final = ability.value + abilityModifer;
      ability.bonus = Number(ability.final) - Number(10);
    }
  }

  /**
   * Prepare generic data
   */
  _prepareBaseStats(actorData) {
    const data = actorData.data;

    data.attributes.healingRate.final = data.attributes.healingRate.value;
    data.attributes.health.final = data.attributes.health.value;
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;

    if (data.inventorySlots.encumbered) {
      data.attributes.speed.modifier.push(-20);
    }

    // Calculate speed
    const speedModifier = Object.values(data.attributes.speed.modifier).reduce((val, mod) => val += mod, 0);
    data.attributes.speed.final = data.attributes.speed.value + speedModifier;

    data.attributes.speed.modifier = [];

    // Determine character armour by what they're wearing
    const [armour, armourBonus] = actorData.items.reduce((arr, item) => {
      if (item.type === "armour") {
        const isActive = getProperty(item.data, "equipped");

        if (isActive) {
          if (item.data.hasOwnProperty("defence")) {
            if (["+", "-"].includes(item.data.defence[0])) {
              let delta = parseInt(item.data.defence);
              arr[1] += delta;
            } else if (item.data.defence[0] === "=") {
              arr[0] = parseInt(item.data.defence.slice(1));
            }
            else {
              console.log("error");
            }
          }
          else {
            console.log("Why does an armour item not have defence property?");
          }
        }
      }
      return arr;
    }, [11, 0]);
    data.attributes.armour.value = armour + armourBonus;

    const xpPercent = this._CalculateXP(data);

    if (xpPercent == 100) {
      // Let's check if we've just reached the max xp and need to level up
      if (data.hasOwnProperty("canLevelUp") && data.canLevelUp) {
        // okay, we've clicked the level up button
        if (data.hasOwnProperty("levelingUp") && data.levelingUp) {
          // well okay lets see if we've improved 3 skills and increased our health
          if (data.remainingAbilities == 0 && !data.canImproveHealth) {
            data.canLevelUp = false;
            data.levelingUp = false;

            const xp = data.details.xp;
            xp.value = 0;

            this._CalculateXP(data);
          }
        }
      }
      else {
        data.canLevelUp = true;
      }
    }

    this._applyRunes(data)
  }

  /**
   * Prepare NPC type specific data
   */
  _prepareNPCData(actorData) {
    const data = actorData.data;

    // Calculate speed
    data.attributes.speed.final = data.attributes.speed.value ;

    this._applyRunes(data)
  }

  // Returns xp percent
  _CalculateXP(data) {
    const xp = data.details.xp;
    const percent = Math.round(xp.value / 10 * 100);
    xp.percent = Math.clamped(percent, 0, 100);

    return xp.percent;
  }

  _applyRunes(data) {
    const runes = this.data.items.filter(item => item.type === "rune");

    for (let rune of runes) {
      const itemData = rune.data;
      const isActive = getProperty(itemData, "equipped");

      if (isActive) {
        if (itemData.hasOwnProperty("targetPath") && itemData.hasOwnProperty("modifier")) {
          const property = getProperty(data, `${itemData.targetPath}`) || {};

          if (["+", "-"].includes(itemData.modifier[0])) {
            let delta = parseInt(itemData.modifier);
            property.final = property.value + delta;
          } else if (itemData.modifier[0] === "=") {
            property.final = parseInt(itemData.modifier.slice(1));
          }
          else {
            console.log("error");
          }
        }
      }
    }
  }

  /**
   * Convert all carried currency to the highest possible denomination to reduce the number of raw coins being
   * carried by an Actor.
   * @return {Promise<Actor5e>}
   */
  convertCurrency() {
    const curr = duplicate(this.data.data.currency);
    const convert = {
      cp: { into: "sp", each: 100 },
      sp: { into: "gp", each: 10 },
    };
    for (let [c, t] of Object.entries(convert)) {
      let change = Math.floor(curr[c] / t.each);
      curr[c] -= (change * t.each);
      curr[t.into] += change;
    }
    return this.update({ "data.currency": curr });
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers
  /* -------------------------------------------- */

  /** @override */
  static async create(data, options = {}) {
    data.token = data.token || {};
    if (data.type === "character") {
      mergeObject(data.token, {
        vision: true,
        dimSight: 30,
        brightSight: 0,
        actorLink: true,
        disposition: 1,
        displayName: 30,
        displayBars: 40,
        bar1: { attribute: "attributes.health" }
      }, { overwrite: false });
    }
    else if (data.type === "npc") {
      mergeObject(data.token, {
        vision: true,
        dimSight: 30,
        brightSight: 0,
        actorLink: false,
        disposition: 0,
        displayName: 40,
        displayBars: 40,
        bar1: { attribute: "attributes.health" }
      }, { overwrite: false });
    }
    else if (data.type === "enemy") {
      mergeObject(data.token, {
        vision: true,
        dimSight: 30,
        brightSight: 0,
        actorLink: false,
        disposition: -1,
        displayName: 40,
        displayBars: 40,
        bar1: { attribute: "attributes.health" }
      }, { overwrite: false });
    }

    return super.create(data, options);
  }

  /* -------------------------------------------- */

  /** @override */
  async modifyTokenAttribute(attribute, value, isDelta, isBar) {
    if (attribute !== "attributes.health") return super.modifyTokenAttribute(attribute, value, isDelta, isBar);

    // Get current and delta health
    const health = getProperty(this.data.data, attribute);
    const max = health.max;

    return this.update({
      "data.attributes.health.value": Math.clamped(health.value, 0, max)
    });
  }

  /* -------------------------------------------- */
  /*  Gameplay Mechanics                          */
  /* -------------------------------------------- */

  /**
   * Apply a certain amount of damage or healing to the health pool for Actor
   * @param {number} amount       An amount of damage (positive) or healing (negative) to sustain
   * @param {number} multiplier   A multiplier which allows for resistance, vulnerability, or healing
   * @return {Promise<Actor>}     A Promise which resolves once the damage has been applied
   */
  async applyDamage(amount=0, multiplier=1) {
    amount = Math.floor(parseInt(amount) * multiplier);

    const health = this.data.data.attributes.health;
    const dh = Math.clamped(health.value - amount, 0, health.max);

    // Update the Actor
    return this.update({
      "data.attributes.health.value": dh
    });
  }
}