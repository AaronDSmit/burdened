/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class BurdenedActorSheetNPC extends ActorSheet {
  
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["burdened", "sheet", "actor", "character"],
      template: "systems/burdened/templates/actor/npc.html",
      width: 720,
      height: 680,
      scrollY: [
        ".inventory .inventory-list",
      ],
      tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {

    // Basic data
    let isOwner = this.entity.owner;
    const data = {
      owner: isOwner,
      limited: this.entity.limited,
      options: this.options,
      isCharacter: this.entity.data.type === "character",
      isNPC: this.entity.data.type === "npc",
      editable: this.isEditable,
      config: CONFIG.BURDENED,
    };

    data.actor = duplicate(this.actor.data);
    data.items = this.actor.items.map(i => {
      i.data.labels = i.labels;
      return i.data;
    });
    data.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));
    data.data = data.actor.data;
    data.labels = this.actor.labels || {};

    // Ability Scores
    for (let [key, ability] of Object.entries(data.actor.data.abilities)) {
      ability.label = CONFIG.BURDENED.abilities[key];
    }

    // Prepare owned items
    this._prepareItems(data);

    return data;
  }


  /** @override */
  async _onDrop(event) {

    // Try to extract the data
    let data;
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'));
      if (data.type !== "Item") return;
    } catch (err) {
      return false;
    }

    const actorData = this.actor.data;

    if (!actorData.data.inventorySlots.encumbered) {
      return super._onDrop(event);
    }

    return false;
  }

  /**
   * Organize and classify Owned Items for Character sheets
   * @private
   */
  _prepareItems(actorData) {

    // Categorize items as inventory, spellbook, features, and classes
    const inventory = {
      weapon: { label: "BURDENED.ItemTypeWeaponPl", items: [], dataset: { type: "weapon" } },
      armour: { label: "BURDENED.ItemTypeArmourPl", items: [], dataset: { type: "armour" } },
      light: { label: "BURDENED.ItemTypeLightPl", items: [], dataset: { type: "light" } },
      spell: { label: "BURDENED.ItemTypeSpellPl", items: [], dataset: { type: "spell" } },
      rune: { label: "BURDENED.ItemTypeRunePl", items: [], dataset: { type: "rune" } },
      item: { label: "BURDENED.ItemTypeItemPl", items: [], dataset: { type: "item" } }
    };

    // Partition items by category
    let [items] = actorData.items.reduce((arr, item) => {

      // Item details
      item.img = item.img || DEFAULT_TOKEN;
      item.isStack = item.data.canStack && Number.isNumeric(item.data.quantity) && (item.data.quantity !== 1);

      // Weapon details
      item.damage = item.data.damage;

      // Item usage
      item.hasUses = item.data.uses && (item.data.uses.max > 0);

      // Item toggle state
      this._prepareItemToggleState(item);

      // Item toggle state
      //this._prepareItemToggleState(item);

      if (Object.keys(inventory).includes(item.type)) {
        arr[0].push(item);
      }
      else {
        console.log("What is this item");
      }

      return arr;
    }, [[]]);

    // Apply active item filters
    // items = this._filterItems(items, this._filters.inventory);

    // Organize Inventory
    let usedSlots = 0;
    for (let i of items) {
      inventory[i.type].items.push(i);
      usedSlots += i.data.slots;
    }
    this._calculateInventorySlots(actorData, usedSlots);

    actorData.inventory = Object.values(inventory);
  }

  /* -------------------------------------------- */

  /**
   * A helper method to establish the displayed preparation state for an item
   * @param {Item} item
   * @private
   */
  _prepareItemToggleState(item) {
    const isActive = getProperty(item.data, "equipped");
    item.toggleClass = isActive ? "active" : "";
    item.toggleTitle = game.i18n.localize(isActive ? "BURDENED.Equipped" : "BURDENED.Unequipped");
  }

  /* -------------------------------------------- */

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    super.activateListeners(html);

    // Item summaries
    html.find('.item .item-name h4').click(event => this._onItemSummary(event));

    // Everything below here is only needed if the sheet is editable
    if (this.isEditable) {

      // Input focus and update
      const inputs = html.find("input");
      inputs.focus(ev => ev.currentTarget.select());
      inputs.addBack().find('[data-dtype="Number"]').change(this._onChangeInputDelta.bind(this));

      // Add Inventory Item
      html.find('.item-create').click(this._onItemCreate.bind(this));

      // Update Inventory Item
      html.find('.item-edit').click(ev => {
        const li = $(ev.currentTarget).parents(".item");
        const item = this.actor.getOwnedItem(li.data("itemId"));
        item.sheet.render(true);
      });

      // Delete Inventory Item
      html.find('.item-delete').click(ev => {
        const li = $(event.currentTarget).parents(".item");
        this.actor.deleteOwnedItem(li.data("itemId"));
        li.slideUp(200, () => this.render(false));
      });

      // Item State Toggling
      html.find('.item-toggle').click(this._onToggleItem.bind(this));

      // Inventory Functions
      html.find(".currency-convert").click(this._onConvertCurrency.bind(this));

      // Rollable abilities.
      html.find('.rollable').click(this._onRoll.bind(this));

      // Item Rolling
      html.find('.item .item-image').click(event => this._onItemRoll(event));

      /* Leveling stuff here */

      // Level Up button
      html.find('.level-up').click(this._onLevelUp.bind(this));

      // Leveling up an ablity
      html.find('.ability-score').click(this._improveAbility.bind(this));

      // Leveling up health
      html.find('.roll-health').click(this._onRollHPFormula.bind(this));

      /* Leveling stuff ends here */
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle input changes to numeric form fields, allowing them to accept delta-typed inputs
   * @param event
   * @private
   */
  _onChangeInputDelta(event) {
    const input = event.target;
    const value = input.value;

    // we're displaying .final versions of data but we actually want to change the .value instead
    input.name = input.name.replace("final", "value");

    if (["+", "-"].includes(value[0])) {
      let delta = parseFloat(value);
      input.value = getProperty(this.actor.data, fieldName) + delta;
    } else if (value[0] === "=") {
      input.value = value.slice(1);
    }
  }

  /* -------------------------------------------- */

  /**
   * Handle rolling of an item from the Actor sheet, obtaining the Item instance and dispatching to it's roll method
   * @private
   */
  _onItemSummary(event) {
    event.preventDefault();
    let li = $(event.currentTarget).parents(".item"),
      item = this.actor.getOwnedItem(li.data("item-id")),
      chatData = item.getChatData({ secrets: this.actor.owner });

    // Toggle summary
    if (li.hasClass("expanded")) {
      let summary = li.children(".item-summary");
      summary.slideUp(200, () => summary.remove());
    } else {
      let div = $(`<div class="item-summary">${chatData.description}</div>`);
      let props = $(`<div class="item-properties"></div>`);
      chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));
      div.append(props);
      li.append(div.hide());
      div.slideDown(200);
    }
    li.toggleClass("expanded");
  }

  /* -------------------------------------------- */

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }

  /**
  * Handle toggling the state of an Owned Item within the Actor
  * @param {Event} event   The triggering click event
  * @private
  */
  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.getOwnedItem(itemId);
    const attr = "data.equipped";
    return item.update({ [attr]: !getProperty(item.data, attr) });
  }

  /* -------------------------------------------- */

  /**
   * Handle rolling of an item from the Actor sheet, obtaining the Item instance and dispatching to it's roll method
   * @private
   */
  _onItemRoll(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.getOwnedItem(itemId);

    // Roll spells through the actor
    //if ( item.data.type === "spell" ) {
    //  return this.actor.useSpell(item, {configureDialog: !event.shiftKey});
    //}

    // Otherwise roll the Item directly
    return item.roll();
  }

  /* -------------------------------------------- */

  _onLevelUp(event) {
    event.preventDefault();
    const data = this.actor.data.data;
    const newLevel = data.details.level + 1;

    return this.actor.update({ "data.levelingUp": true, "data.canImproveHealth": true, "data.remainingAbilities": 3, "data.improvedAbilities": [], "data.details.level": newLevel });
  }

  _improveAbility(event) {
    const element = event.currentTarget;
    const abilityName = element.name;

    const data = this.actor.data.data;

    for (let [key, ability] of Object.entries(data.abilities)) {

      if (key == abilityName) {
        const improvedAbilities = duplicate(data.improvedAbilities);
        improvedAbilities.push(abilityName);

        return this.actor.update({
          [`data.abilities.${key}.value`]: Number(ability.value + 1),
          [`data.abilities.${key}.final`]: ability.value,
          [`data.abilities.${key}.bonus`]: Number(ability.final) - Number(10),
          "data.remainingAbilities": Number(data.remainingAbilities - 1),
          "data.improvedAbilities": improvedAbilities
        });
      }
    }
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {
      let roll = new Roll(dataset.roll, this.actor.data.data).roll();
      let label = dataset.label ? `Rolling ${dataset.label}` : '';
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: this.actor }),
        flavor: label
      });
    }
  }

  /**
   * Handle rolling NPC health values using the provided formula
   * @param {Event} event     The original click event
   * @private
   */
  _onRollHPFormula(event) {
    event.preventDefault();

    const data = this.actor.data.data;

    let roll = new Roll(data.details.level + "d8").roll();
    let label = "Rolling health"
    roll.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: label
    });

    const newHealth = Math.max(roll.total, data.attributes.health.max + 1);

    //AudioHelper.play({src: CONFIG.sounds.dice});
    return this.actor.update({ "data.attributes.health.value": newHealth, "data.attributes.health.max": newHealth, "data.canImproveHealth": false });
  }


  _calculateInventorySlots(actorData, usedSlots) {
    const data = actorData.data;

    const maxSlots = Math.min(data.abilities.con.final, CONFIG.BURDENED.encumbrance.maxSlots);

    // Calculate how many slots our currency is taking up
    if (game.settings.get("burdened", "currencyWeight")) {
      const currency = data.currency;
      const numCoins = Object.values(currency).reduce((val, denom) => val += denom, 0);
      usedSlots += Math.ceil(numCoins / CONFIG.BURDENED.encumbrance.currencyPerSlot);
    }

    // Compute Encumbrance percentage
    const percent = Math.min(usedSlots * 100 / maxSlots, 99);
    const encumbered = usedSlots > maxSlots;

    const inventorySlots = duplicate(data.inventorySlots);
    inventorySlots.value = usedSlots;
    inventorySlots.max = maxSlots;
    inventorySlots.percent = percent;
    inventorySlots.encumbered = encumbered;

    return this.actor.update({ "data.inventorySlots": inventorySlots });
  }

  /* -------------------------------------------- */

  /**
   * Handle mouse click events to convert currency to the highest possible denomination
   * @param {MouseEvent} event    The originating click event
   * @private
   */
  async _onConvertCurrency(event) {
    event.preventDefault();
    return Dialog.confirm({
      title: `${game.i18n.localize("BURDENED.CurrencyConvert")}`,
      content: `<p>${game.i18n.localize("BURDENED.CurrencyConvertHint")}</p>`,
      yes: () => this.actor.convertCurrency()
    });
  }
}