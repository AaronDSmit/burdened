export class BurdenedCombat {

  static rollInitiative(combat, data) {
    // Check teams
    data.combatants = [];
    let teams = {};
    combat.data.combatants.forEach((cbt) => {
      teams[cbt.flags.burdened.team] = { present: true };
      data.combatants.push(cbt);
    });

    // Roll init
    let roll = new Roll("1d6").roll();

    if (roll.total > 3) {
      teams[CONFIG.BURDENED.teams.player].initiative = 1;
      if (teams[CONFIG.BURDENED.teams.other] != null) {
        teams[CONFIG.BURDENED.teams.other].initiative = 2;
      }
      teams[CONFIG.BURDENED.teams.enemy].initiative = 3;
    }
    else {
      teams[CONFIG.BURDENED.teams.player].initiative = 2;
      if (teams[CONFIG.BURDENED.teams.other] != null) {
        teams[CONFIG.BURDENED.teams.other].initiative = 3;
      }
      teams[CONFIG.BURDENED.teams.enemy].initiative = 1;
    }

    let winningTeam = roll.total < 3 ? CONFIG.BURDENED.teams.enemy : CONFIG.BURDENED.teams.player;

    roll.toMessage({
      flavor: game.i18n.format("BURDENED.roll.initiative", { team: winningTeam }),
    });

    // Set init
    for (let i = 0; i < data.combatants.length; ++i) {
      if (!data.combatants[i].actor) {
        return;
      }

      data.combatants[i].initiative = teams[data.combatants[i].flags.burdened.team].initiative;
    }
  }

  static format(object, html, user) {
    html.find(".initiative").each((_, span) => {
      span.innerHTML =
        span.innerHTML == "-789.00"
          ? '<i class="fas fa-weight-hanging"></i>'
          : span.innerHTML;
      span.innerHTML =
        span.innerHTML == "-790.00"
          ? '<i class="fas fa-dizzy"></i>'
          : span.innerHTML;
    });

    html.find('.combat-control[data-control="rollNPC"]').remove();
    html.find('.combat-control[data-control="rollAll"]').remove();
    let trash = html.find(
      '.encounters .combat-control[data-control="endCombat"]'
    );
    $(
      '<a class="combat-control" data-control="reroll"><i class="fas fa-dice"></i></a>'
    ).insertBefore(trash);

    html.find(".combatant").each((_, ct) => {
      // Can't roll individual inits
      $(ct).find(".roll").remove();

      // Get teams color
      let cmbtant = object.combat.getCombatant(ct.dataset.combatantId);
      let team = cmbtant.flags.burdened.team;
      let colour = "yellow"
      let title = cmbtant.flags.burdened.team;

      if (cmbtant.flags.burdened.team == CONFIG.BURDENED.teams.player) {
        colour = "green";
      }
      else if (cmbtant.flags.burdened.team == CONFIG.BURDENED.teams.enemy) {
        colour = "red";
      }

      // Append colored flag
      let controls = $(ct).find(".combatant-controls");
      controls.prepend(
        `<a class='combatant-control flag' style='color:${colour}' title="${title}"><i class='fas fa-flag'></i></a>`
      );
    });
    BurdenedCombat.addListeners(html);
  }

  static addListeners(html) {
    // Cycle through colors
    html.find(".combatant-control.flag").click((ev) => {
      if (!game.user.isGM) {
        return;
      }
      let currentColour = ev.currentTarget.style.color;
      let colours = Object.keys(CONFIG.BURDENED.colours);
      let index = colours.indexOf(currentColour);
      if (index + 1 == colours.length) {
        index = 0;
      } else {
        index++;
      }
      let id = $(ev.currentTarget).closest(".combatant")[0].dataset.combatantId;

      let updatedTeam = CONFIG.BURDENED.teams.other;
      if (currentColour == "green") {
        updatedTeam = CONFIG.BURDENED.teams.player;
      }
      else if (currentColour == "red") {
        updatedTeam = CONFIG.BURDENED.teams.enemy;
      }

      game.combat.updateCombatant({
        _id: id,
        flags: { burdened: { team: updatedTeam } },
      });
    });

    html.find('.combat-control[data-control="reroll"]').click((ev) => {
      if (!game.combat) {
        return;
      }
      let data = {};
      BurdenedCombat.rollInitiative(game.combat, data);
      game.combat.update({ data: data });
    });
  }

  static addCombatant(combat, data, options, id) {
    let token = canvas.tokens.get(data.tokenId);
    let teamId = CONFIG.BURDENED.teams.other;
    switch (token.data.disposition) {
      case -1:
        teamId = CONFIG.BURDENED.teams.enemy;
        break;
      case 0:
        teamId = CONFIG.BURDENED.teams.other; // just for now, nuetral is with players
        break;
      case 1:
        teamId = CONFIG.BURDENED.teams.player;
        break;
    }
    data.flags = {
      burdened: {
        team: teamId,
      },
    };
  }
}