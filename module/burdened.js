// Import Modules
import * as config from "./config.js";
import { registerSystemSettings } from "./settings.js";
import { registerHelpers } from "./helpers.js";
import { preloadHandlebarsTemplates } from "./templates.js";
import { BurdenedCombat } from "./combat.js";
import { Deselection } from "./Deselection.js"

// Import Entities
import { BurdenedActor } from "./actor/actor.js";
import { BurdenedItem } from "./item/item.js";

// Import Applications
import { BurdenedActorSheetCharacter } from "./actor/sheets/character.js";
import { BurdenedActorSheetNPC } from "./actor/sheets/npc.js";
import { BurdenedItemSheet } from "./item/item-sheet.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function () {

  // Place our classes in their own namespace for later reference.
  game.burdened = {
    applications: {
      BurdenedActorSheetCharacter,
      BurdenedActorSheetNPC,
      BurdenedItemSheet
    },
    entities: {
      BurdenedActor,
      BurdenedItem
    }
  };

  // Record Configuration Values
  CONFIG.BURDENED = config.BURDENED;
  CONFIG.Actor.entityClass = BurdenedActor;
  CONFIG.Item.entityClass = BurdenedItem;

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "1d6",
    decimals: 2,
  };

  // Custom Handlebars helpers
  registerHelpers();

  // Register System Settings
  registerSystemSettings();

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("burdened", BurdenedActorSheetCharacter, { types: ["character"], makeDefault: true });
  Actors.registerSheet("burdened", BurdenedActorSheetNPC, { types: ["npc"], makeDefault: true });
  Actors.registerSheet("burdened", BurdenedActorSheetNPC, { types: ["enemy"], makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("burdened", BurdenedItemSheet, { makeDefault: true });

  Deselection.init();

  // Preload Handlebars Templates
  await preloadHandlebarsTemplates();
});

/* -------------------------------------------- */
/*  Foundry VTT Setup                           */
/* -------------------------------------------- */

/**
 * This function runs after game data has been requested and loaded from the servers, so entities exist
 */
Hooks.once("setup", function () {
  // Localize CONFIG objects once up-front
  const toLocalize = [
    "abilities", "currencies","weaponTypes", "weaponProperties","armourTypes", "armourProperties", "rarity", "teams", "colors",  "alignments", "itemActionTypes", "damageTypes"
  ];

  // Exclude some from sorting where the default order matters
  const noSort = [
    "abilities", "currencies", "weaponTypes", "armourTypes", "rarity", "itemActionTypes", "weaponTypes", "damageTypes"
  ];

  // Localize and sort CONFIG objects
  for (let o of toLocalize) {
    const localized = Object.entries(CONFIG.BURDENED[o]).map(e => {
      return [e[0], game.i18n.localize(e[1])];
    });
    if (!noSort.includes(o)) localized.sort((a, b) => a[1].localeCompare(b[1]));
    CONFIG.BURDENED[o] = localized.reduce((obj, e) => {
      obj[e[0]] = e[1];
      return obj;
    }, {});
  }
});

/**
* Once the entire VTT framework is initialized, check to see if we should perform a data migration
*/
Hooks.once("ready", function () {

  // Determine whether a system migration is required and feasible
  //const currentVersion = game.settings.get("burdened", "systemMigrationVersion");
  //const NEEDS_MIGRATION_VERSION = 0.84;
  //const COMPATIBLE_MIGRATION_VERSION = 0.80;
  //let needMigration = (currentVersion < NEEDS_MIGRATION_VERSION) || (currentVersion === null);
  //
  //// Perform the migration
  //if (needMigration && game.user.isGM) {
  //  if (currentVersion && (currentVersion < COMPATIBLE_MIGRATION_VERSION)) {
  //    ui.notifications.error(`Your DnD5e system data is from too old a Foundry version and cannot be reliably migrated to the latest version. The process will be attempted, but errors may occur.`, { permanent: true });
  //  }
  //  migrations.migrateWorld();
  //}
});

Hooks.on("preCreateCombatant", (combat, data, options, id) => {
  BurdenedCombat.addCombatant(combat, data, options, id);
});

Hooks.on("renderCombatTracker", (object, html, data) => {
  BurdenedCombat.format(object, html, data);
});

Hooks.on("preUpdateCombat", async (combat, data, diff, id) => {
  if (!data.round) {
    return;
  }
  BurdenedCombat.rollInitiative(combat, data, diff, id);
});