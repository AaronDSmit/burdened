/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {

  // Define template paths to load
  const templatePaths = [

    // Actor Sheet Partials
    "systems/burdened/templates/actor/parts/actor-inventory.html",

    // Item Sheet Partials
    "systems/burdened/templates/item/parts/item-description.html",
    "systems/burdened/templates/item/parts/item-mountable.html",
    "systems/burdened/templates/item/parts/item-action.html",
    "systems/burdened/templates/item/parts/item-header.html"
  ];

  // Load the template parts
  return loadTemplates(templatePaths);
};
