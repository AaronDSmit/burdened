export const registerSystemSettings = function() {

  /**
   * Track the system version upon which point a migration was last applied
   */
  game.settings.register("burdened", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: false,
    type: Number,
    default: 0
  });

  /**
   * Require Currency Carrying Weight
   */
  game.settings.register("burdened", "currencyWeight", {
    name: "SETTINGS.burdenedCurWtN",
    hint: "SETTINGS.burdenedCurWtL",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  /**
   * Option to automatically collapse Item Card descriptions
   */
  game.settings.register("burdened", "autoCollapseItemCards", {
    name: "SETTINGS.burdenedAutoCollapseCardN",
    hint: "SETTINGS.burdenedAutoCollapseCardL",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: s => {
      ui.chat.render();
    }
  });

   /**
   * Register Initiative formula setting
   */
  game.settings.register("burdened" ,"groupInitiative", {
    name: "SETTINGS.burdenedGroupInitiative",
    hint: "SETTINGS.burdenedGroupInitiativeHint",
    scope: "world",
    config: false,
    default: true,
    type: Boolean,
  });
};